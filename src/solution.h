#ifndef SEGMENT_INTERSECT_SOLUTION_H
#define SEGMENT_INTERSECT_SOLUTION_H

#include <math.h>
#include <algorithm> // std::min

// Precision threshold for constructing 3D objects
constexpr double tol = 1e-10;

class Vector3D {
public:
    double X;
    double Y;
    double Z;

    Vector3D(const std::initializer_list<double> &init_list) {
        if (init_list.size() != 3) {
            throw std::invalid_argument("Wrong number of args");
        }
        auto it = init_list.begin();
        X = *it++;
        Y = *it++;
        Z = *it;
    }

    bool operator==(const Vector3D &other) const {
        return (std::abs(X - other.X) < tol)
               && (std::abs(Y - other.Y) < tol)
               && (std::abs(Z - other.Z) < tol);
    }

    Vector3D operator*(const Vector3D &other) const {
        return Vector3D{Y * other.Z - Z * other.Y, Z * other.X - X * other.Z, X * other.Y - Y * other.X};
    }

    Vector3D operator*(const double &num) const {
        return Vector3D{X * num, Y * num, Z * num};
    }

    Vector3D operator/(const double &num) const {
        return Vector3D{X / num, Y / num, Z / num};
    }

    Vector3D operator+(const Vector3D &other) const {
        return Vector3D{X + other.X, Y + other.Y, Z + other.Z};
    }

    Vector3D operator-(const Vector3D &other) const {
        return Vector3D{X - other.X, Y - other.Y, Z - other.Z};
    }

    double dot(const Vector3D &other) const {
        return X * other.X + Y * other.Y + Z * other.Z;
    }

    double length() const {
        return std::sqrt(X * X + Y * Y + Z * Z);
    }

    double length_squared(const Vector3D &other) {
        return dot(*this) * other.dot(other) - dot(other) * dot(other);
    }

    double distance(const Vector3D &other) const {
        return (*this - other).length();
    }


    /**
     * @brief Find center between two points
     */
    Vector3D center(const Vector3D& other){
        return (*this+other)/2.;
    }
};

class Segment3D {
public:
    Vector3D start;
    Vector3D end;

    Segment3D(const Vector3D &start, const Vector3D &end) : start(start), end(end) {}

    Vector3D intersect(const Segment3D &other) {
        double segments_dist = distance(other);
        if (segments_dist >= tol) {
            throw std::invalid_argument("Segments do not intersect");
        }

        Vector3D d1 = direction(), d2 = other.direction();
        bool seg1_is_deg = d1.length() < tol, seg2_is_deg = d2.length() < tol;
        if (seg1_is_deg && seg2_is_deg) { return (center() + other.center()) / 2.; }
        else if (seg1_is_deg) { return center(); }
        else if (seg2_is_deg) { return other.center(); }

        Vector3D p2_p1 = other.start - start;
        double len_sq = d1.length_squared(d2);
        if (std::abs(len_sq - 0) < tol) {
            double l1 = length(), l2 = other.length();
            double AP = start.distance(other.start),AQ = start.distance(other.end),
                    BP = end.distance((other.start)),BQ = end.distance(other.end);
            if(AP < tol){
                if(std::abs(BP+BQ-l2)>=tol) return start.center(other.start);
            }
            else if(AQ<tol){
                if(std::abs(BP+BQ-l2)>=tol) return start.center(other.end);
            }
            else if(BP<tol){
                if(std::abs(AP+AQ-l1)>=tol) return end.center(other.start);
            }
            else if(BQ<tol){
                if(std::abs(AP+AQ-l1)>=tol) return end.center(other.end);
            }
            throw std::invalid_argument("The segments do not intersect at exactly one point");
        }
        double t1 = (p2_p1 * d2).dot(d1 * d2) / len_sq;
        Vector3D vec1 = start + d1 * t1;
        return vec1;
    }

    Vector3D direction() const {
        return end - start;
    }

    double length() const {
        return direction().length();
    }

    Vector3D center() const {
        return (start + end) / 2.;
    }

    /**
     * @brief Distance between two segments
     */
    double distance(const Segment3D &other) const {
        double res;
        Vector3D AB = direction(), PQ = other.direction();
        Vector3D AP = other.start - start;
        double a = AB.dot(AB), b = PQ.dot(PQ), c = AB.dot(PQ);
        double d = a * b - c * c, e = AB.dot(AP), f = PQ.dot(AP);
        if (std::abs(d) < tol) {
            res = std::min({other.distance(start),
                            other.distance(end),
                            distance(other.start),
                            distance(other.end)});
        } else {
            double t = (e * c - f * a) / d;
            double s = (e * b - f * c) / d;
            if (s < 0) { res = other.distance(start); }
            else if (s > 1) { res = other.distance(end); }
            else if (t < 0) { res = distance(other.start); }
            else if (t > 1) { res = distance(other.end); }
            else { res = (AP + PQ * t - AB * s).length(); }
        }
        return res;
    }

    /**
     * @brief Distance between segment and point P
     */
    double distance(const Vector3D &P) const {
        Vector3D AB = direction(), AP = P - start;
        if (length() < tol) {
            return center().distance(P);
        }
        Vector3D u = AB / AB.length();
        double alpha = AP.dot(u);
        double d;
        if (alpha < 0) { d = AP.length(); }
        else if (alpha > AB.length()) { d = (P - end).length(); }
        else { d = (AP - u * alpha).length(); }
        return d;
    }
};

#endif //SEGMENT_INTERSECT_SOLUTION_H
