#ifndef SEGMENT_INTERSECT_SOLUTION_TEST_H
#define SEGMENT_INTERSECT_SOLUTION_TEST_H

#include "../solution.h"

namespace solution_test{
    TEST(Vector3D_ctor, 1){
        Vector3D vec1{0,0,0};
        ASSERT_EQ(vec1.X,0);
        ASSERT_EQ(vec1.Y,0);
        ASSERT_EQ(vec1.Z,0);
    }

    TEST(Vector3D_ctor, 2){
        Vector3D vec1{-1,-2,-3};
        ASSERT_EQ(vec1.X,-1);
        ASSERT_EQ(vec1.Y,-2);
        ASSERT_EQ(vec1.Z,-3);
    }

    TEST(Vector3D_ctor, 3){
        ASSERT_THROW(Vector3D({0,0,0,0}),std::invalid_argument);
    }

    TEST(Vector3D_ctor, 4){
        ASSERT_THROW(Vector3D({0,0}),std::invalid_argument);
    }

    TEST(Segment3D_distance_Vector3D, 1){
        Segment3D seg1{{0,0,0},{1,0,0}};
        Vector3D vec1{2,0,0};
        Vector3D vec2{0,1,0};
        Vector3D vec3{0,-1,0};
        Vector3D vec4{-1,0,0};
        Vector3D vec5{1,0,1};
        Vector3D vec6{1,0,-1};
        double ans = 1;
        ASSERT_NEAR(seg1.distance(vec1),ans,tol);
        ASSERT_NEAR(seg1.distance(vec2),ans,tol);
        ASSERT_NEAR(seg1.distance(vec3),ans,tol);
        ASSERT_NEAR(seg1.distance(vec4),ans,tol);
        ASSERT_NEAR(seg1.distance(vec5),ans,tol);
        ASSERT_NEAR(seg1.distance(vec6),ans,tol);
    }

    // point lies on segment
    TEST(Segment3D_distance_Vector3D, 2){
        Segment3D seg1{{0,0,0},{1,1,1}};
        Vector3D vec1{0.1,0.1,0.1};
        Vector3D vec2{0.5,0.5,0.5};
        Vector3D vec3{0.8,0.8,0.8};
        double ans = 0;
        ASSERT_NEAR(seg1.distance(vec1),ans,tol);
        ASSERT_NEAR(seg1.distance(vec2),ans,tol);
        ASSERT_NEAR(seg1.distance(vec3),ans,tol);
    }

    // point lies on edge of segment
    TEST(Segment3D_distance_Vector3D, 3){
        Segment3D seg1{{0,0,0},{1,1,1}};
        Vector3D vec1{0,0,0};
        Vector3D vec2{1,1,1};
        double ans = 0;
        ASSERT_NEAR(seg1.distance(vec1),ans,tol);
        ASSERT_NEAR(seg1.distance(vec2),ans,tol);
    }

    TEST(Segment3D_distance_Vector3D, 4){
        Segment3D seg1{{1,0,0},{2,0,0}};
        Vector3D vec1{0,0,0};
        Vector3D vec2{4,0,0};
        ASSERT_NEAR(seg1.distance(vec1),1,tol);
        ASSERT_NEAR(seg1.distance(vec2),2,tol);
    }

    TEST(Segment3D_distance_Vector3D, 5){
        Segment3D seg1{{0,0,0},{1,1,1}};
        Vector3D vec1{2,2,2};
        Vector3D vec2{-1,-1,-1};
        double ans = std::sqrt(3);
        ASSERT_NEAR(seg1.distance(vec1),ans,tol);
        ASSERT_NEAR(seg1.distance(vec2),ans,tol);
    }

    // segment are degenerate
    TEST(Segment3D_distance_Vector3D, 6){
        Segment3D seg1{{1,1,1},{1,1,1}};
        Vector3D vec1{1,1,0};
        Vector3D vec2{1,0,1};
        Vector3D vec3{0,1,1};
        double ans = 1;
        ASSERT_NEAR(seg1.distance(vec1),ans,tol);
        ASSERT_NEAR(seg1.distance(vec2),ans,tol);
        ASSERT_NEAR(seg1.distance(vec3),ans,tol);
    }

    TEST(Segment3D_distance_Vector3D, 7){
        Segment3D seg1{{1,1,1},{-1,-1,-1}};
        Vector3D vec1{2,2,2};
        Vector3D vec2{-2,-2,-2};
        double ans = std::sqrt(3);
        ASSERT_NEAR(seg1.distance(vec1),ans,tol);
        ASSERT_NEAR(seg1.distance(vec2),ans,tol);
    }

    TEST(Segment3D_distance_Vector3D, 8){
        Segment3D seg1{{0,0,0},{0,0,0}};
        Vector3D vec1{1,0,0};
        Vector3D vec2{-1,0,0};
        Vector3D vec3{0,1,0};
        Vector3D vec4{0,-1,0};
        Vector3D vec5{0,0,1};
        Vector3D vec6{0,0,-1};
        double ans = 1;
        ASSERT_NEAR(seg1.distance(vec1),ans,tol);
        ASSERT_NEAR(seg1.distance(vec2),ans,tol);
        ASSERT_NEAR(seg1.distance(vec3),ans,tol);
        ASSERT_NEAR(seg1.distance(vec4),ans,tol);
        ASSERT_NEAR(seg1.distance(vec5),ans,tol);
        ASSERT_NEAR(seg1.distance(vec6),ans,tol);
    }


    // segments intersect
    TEST(Segment3D_distance,1){
        Segment3D seg1{{-1,0,0},{1,0,0}};
        Segment3D seg2{{0,-1,0},{0,1,0}};
        ASSERT_TRUE(seg1.distance(seg2) < tol);
    }

// segments intersect
    TEST(Segment3D_distance,2){
        Segment3D seg1{{0,0,1},{1,0,0}};
        Segment3D seg2{{0,0,0},{1,0,1}};
        ASSERT_TRUE(seg1.distance(seg2) < tol);
    }

// segments intersect
// the segments lie on the same line but have one common point
    TEST(Segment3D_distance,3){
        Segment3D seg1{{0,0,0},{1,1,1}};
        Segment3D seg2{{1,1,1},{2,2,2}};
        ASSERT_TRUE(seg1.distance(seg2) < tol);
    }

// segments intersect
    TEST(Segment3D_distance,4){
        Segment3D seg1{{0,0,0},{1,0,0}};
        Segment3D seg2{{0,0,0},{0,1,0}};
        Segment3D seg3{{0,0,0},{0,0,1}};
        ASSERT_TRUE(seg1.distance(seg2) < tol);
        ASSERT_TRUE(seg1.distance(seg3) < tol);
        ASSERT_TRUE(seg3.distance(seg1) < tol);
    }

// Segments have multiple common points
    TEST(Segment3D_distance, 5){
        Segment3D seg1{{0,0,0},{2,0,0}};
        Segment3D seg2{{1,0,0},{3,0,0}};
        ASSERT_TRUE(seg1.distance(seg2) < tol);
    }

// Segments have multiple common points
    TEST(Segment3D_distance, 6){
        Segment3D seg1{{0,0,0},{2,0,0}};
        Segment3D seg2{{3,0,0},{1,0,0}};
        ASSERT_TRUE(seg1.distance(seg2) < tol);
    }

// Segments have multiple common points
    TEST(Segment3D_distance, 7){
        Segment3D seg1{{2,0,0},{0,0,0}};
        Segment3D seg2{{1,0,0},{3,0,0}};
        ASSERT_TRUE(seg1.distance(seg2) < tol);
    }

    TEST(Segment3D_distance, 8){
        Segment3D seg1{{2,0,0},{0,0,0}};
        Segment3D seg2{{3,0,0},{1,0,0}};
        ASSERT_TRUE(seg1.distance(seg2) < tol);
    }

// Segments have multiple common points
    TEST(Segment3D_distance, 9){
        Segment3D seg1{{1,0,0},{3,0,0}};
        Segment3D seg2{{0,0,0},{2,0,0}};
        ASSERT_TRUE(seg1.distance(seg2) < tol);
    }

// Segments have multiple common points
    TEST(Segment3D_distance, 10){
        Segment3D seg1{{3,0,0},{1,0,0}};
        Segment3D seg2{{0,0,0},{2,0,0}};
        ASSERT_TRUE(seg1.distance(seg2) < tol);
    }

// Segments have multiple common points
    TEST(Segment3D_distance, 11){
        Segment3D seg1{{1,0,0},{3,0,0}};
        Segment3D seg2{{2,0,0},{0,0,0}};
        ASSERT_TRUE(seg1.distance(seg2) < tol);
    }

// Segments have multiple common points
    TEST(Segment3D_distance, 12){
        Segment3D seg1{{3,0,0},{1,0,0}};
        Segment3D seg2{{2,0,0},{0,0,0}};
        ASSERT_TRUE(seg1.distance(seg2) < tol);
    }

// Segments have multiple common points
    TEST(Segment3D_distance, 13){
        Segment3D seg1{{0,0,0},{4,0,0}};
        Segment3D seg2{{1,0,0},{2,0,0}};
        ASSERT_TRUE(seg1.distance(seg2) < tol);
    }

// Segments have multiple common points
    TEST(Segment3D_distance, 14){
        Segment3D seg1{{1,0,0},{2,0,0}};
        Segment3D seg2{{0,0,0},{4,0,0}};
        ASSERT_TRUE(seg1.distance(seg2) < tol);
    }

// Segments are equal
    TEST(Segment3D_distance, 15){
        Segment3D seg1{{1,2,3},{2,3,4}};
        Segment3D seg2{{1,2,3},{2,3,4}};
        ASSERT_TRUE(seg1.distance(seg2) < tol);
    }

// Intersection of a degenerate segment with a non-degenerate one
    TEST(Segment3D_distance, 16){
        Segment3D seg1{{0,0,0},{0,0,0}};
        Segment3D seg2{{-1,0,0},{1,0,0}};
        ASSERT_TRUE(seg1.distance(seg2) < tol);
    }

// Intersection of a degenerate segment with a non-degenerate one
    TEST(Segment3D_distance, 17){
        Segment3D seg1{{0,0,0},{0,0,0}};
        Segment3D seg2{{0,0,0},{1,0,0}};
        ASSERT_TRUE(seg1.distance(seg2) < tol);
    }

// Intersection of degenerate segments
    TEST(Segment3D_distance, 18){
        Segment3D seg1{{0,0,0},{0,0,0}};
        Segment3D seg2{{1,1,1},{1,1,1}};
        ASSERT_TRUE(seg1.distance(seg2) >= tol);
    }

// Intersection of degenerate segments
    TEST(Segment3D_distance, 19){
        Segment3D seg1{{0,0,0},{0,0,0}};
        Segment3D seg2{{0,0,0},{0,0,0}};
        ASSERT_TRUE(seg1.distance(seg2) < tol);
    }

// Intersection of degenerate segments
    TEST(Segment3D_distance, 20){
        Segment3D seg1{{1,1,1},{1,1,1}};
        Segment3D seg2{{1,1,1},{1,1,1}};
        ASSERT_TRUE(seg1.distance(seg2) < tol);
    }

    TEST(Segment3D_ctor, 1){
        Segment3D seg1{{0,0,0},{1,1,1}};
        ASSERT_EQ(seg1.start.X,0);
        ASSERT_EQ(seg1.start.Y,0);
        ASSERT_EQ(seg1.start.Z,0);
        ASSERT_EQ(seg1.end.X,1);
        ASSERT_EQ(seg1.end.Y,1);
        ASSERT_EQ(seg1.end.Z,1);
    }

    // segments intersect
    TEST(Segment3D_intersect,1){
        Segment3D seg1{{-1,0,0},{1,0,0}};
        Segment3D seg2{{0,-1,0},{0,1,0}};
        ASSERT_TRUE(seg1.intersect(seg2) == Vector3D({0,0,0}));
    }

    // segments intersect
    TEST(Segment3D_intersect,2){
        Segment3D seg1{{0,0,1},{1,0,0}};
        Segment3D seg2{{0,0,0},{1,0,1}};
        ASSERT_TRUE(seg1.intersect(seg2) == Vector3D({0.5,0,0.5}));
    }

    // segments intersect
    // the segments lie on the same line but have one common point
    TEST(Segment3D_intersect,3){
        Segment3D seg1{{0,0,0},{1,1,1}};
        Segment3D seg2{{1,1,1},{2,2,2}};
        ASSERT_TRUE(seg1.intersect(seg2) == Vector3D({1,1,1}));
    }

    // segments intersect
    TEST(Segment3D_intersect,4){
        Segment3D seg1{{0,0,0},{1,0,0}};
        Segment3D seg2{{0,0,0},{0,1,0}};
        Segment3D seg3{{0,0,0},{0,0,1}};
        ASSERT_TRUE(seg1.intersect(seg2) == Vector3D({0,0,0}));
        ASSERT_TRUE(seg1.intersect(seg3) == Vector3D({0,0,0}));
        ASSERT_TRUE(seg3.intersect(seg1) == Vector3D({0,0,0}));
    }

    // Segments have multiple common points
    TEST(Segment3D_intersect, 5){
        Segment3D seg1{{0,0,0},{2,0,0}};
        Segment3D seg2{{1,0,0},{3,0,0}};
        ASSERT_THROW(seg1.intersect(seg2),std::invalid_argument);
    }

    // Segments have multiple common points
    TEST(Segment3D_intersect, 6){
        Segment3D seg1{{0,0,0},{2,0,0}};
        Segment3D seg2{{3,0,0},{1,0,0}};
        ASSERT_THROW(seg1.intersect(seg2),std::invalid_argument);
    }

    // Segments have multiple common points
    TEST(Segment3D_intersect, 7){
        Segment3D seg1{{2,0,0},{0,0,0}};
        Segment3D seg2{{1,0,0},{3,0,0}};
        ASSERT_THROW(seg1.intersect(seg2),std::invalid_argument);
    }

    TEST(Segment3D_intersect, 8){
        Segment3D seg1{{2,0,0},{0,0,0}};
        Segment3D seg2{{3,0,0},{1,0,0}};
        ASSERT_THROW(seg1.intersect(seg2),std::invalid_argument);
    }

    // Segments have multiple common points
    TEST(Segment3D_intersect, 9){
        Segment3D seg1{{1,0,0},{3,0,0}};
        Segment3D seg2{{0,0,0},{2,0,0}};
        ASSERT_THROW(seg1.intersect(seg2),std::invalid_argument);
    }

    // Segments have multiple common points
    TEST(Segment3D_intersect, 10){
        Segment3D seg1{{3,0,0},{1,0,0}};
        Segment3D seg2{{0,0,0},{2,0,0}};
        ASSERT_THROW(seg1.intersect(seg2),std::invalid_argument);
    }

    // Segments have multiple common points
    TEST(Segment3D_intersect, 11){
        Segment3D seg1{{1,0,0},{3,0,0}};
        Segment3D seg2{{2,0,0},{0,0,0}};
        ASSERT_THROW(seg1.intersect(seg2),std::invalid_argument);
    }

    // Segments have multiple common points
    TEST(Segment3D_intersect, 12){
        Segment3D seg1{{3,0,0},{1,0,0}};
        Segment3D seg2{{2,0,0},{0,0,0}};
        ASSERT_THROW(seg1.intersect(seg2),std::invalid_argument);
    }

    // Segments have multiple common points
    TEST(Segment3D_intersect, 13){
        Segment3D seg1{{0,0,0},{4,0,0}};
        Segment3D seg2{{1,0,0},{2,0,0}};
        ASSERT_THROW(seg1.intersect(seg2),std::invalid_argument);
    }

    // Segments have multiple common points
    TEST(Segment3D_intersect, 14){
        Segment3D seg1{{1,0,0},{2,0,0}};
        Segment3D seg2{{0,0,0},{4,0,0}};
        ASSERT_THROW(seg1.intersect(seg2),std::invalid_argument);
    }

    // Segments are equal
    TEST(Segment3D_intersect, 15){
        Segment3D seg1{{1,2,3},{2,3,4}};
        Segment3D seg2{{1,2,3},{2,3,4}};
        ASSERT_THROW(seg1.intersect(seg2),std::invalid_argument);
    }


    // Intersection of a degenerate segment with a non-degenerate one
    TEST(Segment3D_intersect, 16){
        Segment3D seg1{{0,0,0},{0,0,0}};
        Segment3D seg2{{-1,0,0},{1,0,0}};
        ASSERT_TRUE(seg1.intersect(seg2) == Vector3D({0,0,0}));

    }

    // Intersection of a degenerate segment with a non-degenerate one
    TEST(Segment3D_intersect, 17){
        Segment3D seg1{{0,0,0},{0,0,0}};
        Segment3D seg2{{0,0,0},{1,0,0}};
        ASSERT_TRUE(seg1.intersect(seg2) == Vector3D({0,0,0}));
    }

    // Intersection of degenerate segments
    TEST(Segment3D_intersect, 18){
        Segment3D seg1{{0,0,0},{0,0,0}};
        Segment3D seg2{{1,1,1},{1,1,1}};
        ASSERT_THROW(seg1.intersect(seg2),std::invalid_argument);
    }

    // Intersection of degenerate segments
    TEST(Segment3D_intersect, 19){
        Segment3D seg1{{0,0,0},{0,0,0}};
        Segment3D seg2{{0,0,0},{0,0,0}};
        ASSERT_TRUE(seg1.intersect(seg2) == Vector3D({0,0,0}));
    }

    // Intersection of degenerate segments
    TEST(Segment3D_intersect, 20){
        Segment3D seg1{{1,1,1},{1,1,1}};
        Segment3D seg2{{1,1,1},{1,1,1}};
        ASSERT_TRUE(seg1.intersect(seg2) == Vector3D({1,1,1}));
    }

    TEST(Segment3D_intersect, 21){
        Segment3D seg1{{0,0,0},{1,1,1}};
        Segment3D seg2{{1,1,1},{1,1,0}};
        Segment3D seg3{{1,1,0},{1,1,1}};
        Segment3D seg4{{1,1,1},{1,0,1}};
        Segment3D seg5{{1,0,1},{1,1,1}};
        Segment3D seg6{{1,1,1},{0,1,1}};
        Segment3D seg7{{0,1,1},{1,1,1}};
        Vector3D vec1{1,1,1};
        ASSERT_TRUE(seg1.intersect(seg2) == vec1);
        ASSERT_TRUE(seg1.intersect(seg3) == vec1);
        ASSERT_TRUE(seg1.intersect(seg4) == vec1);
        ASSERT_TRUE(seg1.intersect(seg5) == vec1);
        ASSERT_TRUE(seg1.intersect(seg6) == vec1);
        ASSERT_TRUE(seg1.intersect(seg7) == vec1);
    }

    TEST(Segment3D_intersect, 22){
        Segment3D seg1{{0,0,0},{1,0,0}};
        Segment3D seg2{{1,0,0},{2,0,0}};
        Segment3D seg3{{2,0,0},{1,0,0}};
        Vector3D vec1{1,0,0};
        ASSERT_TRUE(seg1.intersect(seg2) == vec1);
        ASSERT_TRUE(seg1.intersect(seg3) == vec1);
    }

    TEST(Segment3D_intersect, 23){
        Segment3D seg1{{1,0,0},{0,0,0}};
        Segment3D seg2{{1,0,0},{2,0,0}};
        Segment3D seg3{{2,0,0},{1,0,0}};
        Vector3D vec1{1,0,0};
        ASSERT_TRUE(seg1.intersect(seg2) == vec1);
        ASSERT_TRUE(seg1.intersect(seg3) == vec1);
    }

    TEST(Segment3D_intersect, 24){
        Segment3D seg1{{0,0,0},{2,0,0}};
        Segment3D seg2{{1,0,0},{3,0,0}};
        Segment3D seg3{{3,0,0},{1,0,0}};
        ASSERT_THROW(seg1.intersect(seg2), std::invalid_argument);
        ASSERT_THROW(seg1.intersect(seg3), std::invalid_argument);
    }

    TEST(Segment3D_intersect, 25){
        Segment3D seg1{{2,0,0},{0,0,0}};
        Segment3D seg2{{1,0,0},{3,0,0}};
        Segment3D seg3{{3,0,0},{1,0,0}};
        ASSERT_THROW(seg1.intersect(seg2), std::invalid_argument);
        ASSERT_THROW(seg1.intersect(seg3), std::invalid_argument);
    }

} // namespace solution_test

#endif //SEGMENT_INTERSECT_SOLUTION_TEST_H
